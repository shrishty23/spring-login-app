package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeResource {

	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "home.html";
    }
	
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	    public String login() {
	        return "login.html";
	    }

	@RequestMapping(value = "/user", method = RequestMethod.GET)
    public String user() {
        return "hello.html";
    }

	 @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin() {
        return "admin.html";
    }
	 
	 
}


